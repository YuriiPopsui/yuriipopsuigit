/*
 * Задача 4.
 *
 * Дописать требуемый функционал что бы код работал правильно.
 * 
 * Дана стоимость в виде строки: `"$120"`.
 * Первый символ — валюта, затем – число.
 * Необходимо из такой строки выделять число-значение, в данном случае 120.
 * Обратите внимание что нужно возвращать не строку 120, а именно число 120.
 *
 * Подсказка:
 * - Для проверки на NaN нужно использовать isNaN(); 
 *   https://developer.mozilla.org/ru/docs/orphaned/Web/JavaScript/Reference/Global_Objects/Number/isNaN
*/

const PRICE = '$' + prompt('Введите стоимость продукта'); // ИЗМЕНЯТЬ ЗАПРЕЩЕНО

// РЕШЕНИЕ
let workPrice = PRICE;
let moneySum = workPrice.slice(1);
if (!isNaN(moneySum)) {
  let result = +moneySum;
  console.log(result);
}
else {
  console.log('Error');
}

