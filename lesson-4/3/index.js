/*
 * Задача 3.
 *
 * Дописать требуемый функционал что бы код работал правильно.
 * 
 * Необходимо проверить длину строки в переменной string.
 * Если она превосходит maxLength – заменяет конец string на ... таким образом, чтобы её длина стала равна maxLength.
 * В консоль должна вывестись (при необходимости) усечённая строка.
 * 
 * Пример 1: string -> 'Вот, что мне хотелось бы сказать на эту тему:'
 *         maxLength -> 21
 *         result -> 'Вот, что мне хотел...'
 * 
 * Пример 2: string -> 'Вот, что мне хотелось'
 *         maxLength -> 100
 *         result -> 'Вот, что мне хотелось'
 *
 * Условия:
 * - Переменная string должна обладать типом string;
 * - Переменная maxLength должна обладать типом number.
 * 
 */
const string = prompt('Введите строку, которую нужно сократить:'); // ИЗМЕНЯТЬ ЗАПРЕЩЕНО
const maxLength = prompt('Введите максимальную длинну строки:'); // ИЗМЕНЯТЬ ЗАПРЕЩЕНО

// РЕШЕНИЕ

// Еще можно предусмотреть условие, при котором для строки длиной в 3 символа 
// вывести необрезанную строку. Потому что иначе выведутся просто три точки.
 
if (maxLength !== null) {
  const maxLen = Number(maxLength);

  if (string.length <= maxLen ) {
    console.log(string);
  } else {
    const cuttedString = string.slice(0, (maxLen - 3));
    console.log(`${cuttedString}...`);
  }
} else {
    console.log(string);
}
