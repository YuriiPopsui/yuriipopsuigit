/*
 * Задача 1.
 *
 * Дописать требуемый функционал что бы код работал правильно.
 * 
 * Необходимо преобразовать первый символ строки в заглавный и вывести эту строку в консоль.
 * Пример 1: привет -> Привет
 * 
 * Пример 2: 222 -> Error.
 *
 * Условия:
 * - Необходимо проверить что введенный текст не число иначе выводить ошибку в консоль.
 */

const str = prompt('Введите любую строку:'); // ИЗМЕНЯТЬ ЗАПРЕЩЕНО

// РЕШЕНИЕ

let string = Number(str);

if (isNaN(string)) {
  let st = str;
  let firstLetter = st.slice(0, 1);
  let uppercased = firstLetter.toUpperCase();
  let result = st.replace(firstLetter, uppercased);
  console.log(result);
}
else {
  console.log('Error');
}