/**
 * Доработать форму из 1-го задания.
 * 
 * Добавить обработчик сабмита формы.
 * 
 * Для того что бы увидеть результат откройте index.html файл в браузере.
 * 
 * Обязательно!
 * 1. При сабмите формы страница не должна перезагружаться
 * 2. Генерировать ошибку если пользователь пытается сабмитить форму с пустыми или содержащими только пробел(ы) полями.
 * 3. Если поля формы заполнены и пользователь нажимает кнопку Вход → вывести в консоль объект следующего вида
 * {
 *  email: 'эмейл который ввёл пользователь',
 *  password: 'пароль который ввёл пользователь',
 *  remember: 'true/false'
 * }
*/

// РЕШЕНИЕ

///// Create form ///// 
const element = document.body;
const formElement = document.createElement('form');
formElement.id = 'form';
element.appendChild(formElement);

////  Create Email Div  ////
const formEmail = document.createElement('div');
const labelEmailForm = document.createElement('label');
const inputEmailForm = document.createElement('input');
formEmail.className = 'form-group';

labelEmailForm.innerHTML = 'Электропочта';
labelEmailForm.setAttribute('for', 'email');

inputEmailForm.className = 'form-control';
inputEmailForm.id = 'email';
inputEmailForm.setAttribute('type', 'email');
inputEmailForm.setAttribute('placeholder', 'Введите свою электропочту');

formElement.appendChild(formEmail);
formEmail.appendChild(labelEmailForm);
formEmail.appendChild(inputEmailForm);

//// Create Password Div  ////
const formPassword = document.createElement('div');
const labelPasswordForm = document.createElement('label');
const inputPasswordForm = document.createElement('input');
formPassword.className = 'form-group';

labelPasswordForm.innerHTML = 'Пароль';
labelPasswordForm.setAttribute('for', 'password');

inputPasswordForm.className = 'form-control';
inputPasswordForm.id = 'password';
inputPasswordForm.setAttribute('type', 'password');
inputPasswordForm.setAttribute('placeholder', 'Введите пароль');

formElement.appendChild(formPassword);
formPassword.appendChild(labelPasswordForm);
formPassword.appendChild(inputPasswordForm);

////  Create Checkbox  ////
const formCheckbox = document.createElement('div');
const inputCheckboxForm = document.createElement('input');
const labelCheckboxForm = document.createElement('label');
formCheckbox.className = 'form-group form-check';

labelCheckboxForm.innerHTML = 'Запомнить меня';
labelCheckboxForm.setAttribute('for', 'exampleCheck1');
labelCheckboxForm.className = 'form-check-label';

inputCheckboxForm.className = 'form-check-input';
inputCheckboxForm.id = 'exampleCheck1';
inputCheckboxForm.setAttribute('type', 'checkbox');

formElement.appendChild(formCheckbox);
formCheckbox.appendChild(inputCheckboxForm);
formCheckbox.appendChild(labelCheckboxForm);

//// Create Button ////
const formButton = document.createElement('button');
formButton.innerHTML = 'Вход';
formButton.className = 'btn btn-primary';
formButton.setAttribute('type', 'submit');
formElement.appendChild(formButton);

//// Event Handler  ////
const submitBtn = document.querySelector('.btn');
const submitObject = {
  email: null,
  password: null,
  remember: null
}
submitBtn.addEventListener("click", (event) => {
  event.preventDefault();
  
  if(inputEmailForm.value && inputEmailForm.value !== " " && inputEmailForm.value.trim().length !== 0 && inputPasswordForm.value && inputPasswordForm.value !== " ") {
    submitObject.email = inputEmailForm.value;
    submitObject.password = inputPasswordForm.value;
    if(inputCheckboxForm.checked) {
      submitObject.remember = true;
    }
    else {
      submitObject.remember = false;
    }  
    console.log(submitObject);
  } 
  else {
    throw new Error("Sorry! You can't submit an empty form!");
  }
});