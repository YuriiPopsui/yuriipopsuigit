/**
 * Создать форму динамически при помощи JavaScript.
 * 
 * В html находится пример формы которая должна быть сгенерирована.
 * 
 * Для того что бы увидеть результат откройте index.html файл в браузере.
 * 
 * Обязательно!
 * 1. Для генерации элементов обязательно использовать метод document.createElement
 * 2. Для установки атрибутов элементам обязательно необходимо использовать document.setAttribute
 * 3. Всем созданным элементам необходимо добавить классы как в разметке
 * 4. После того как динамическая разметка будет готова необходимо удалить код в HTML который находится между комментариями
*/

// РЕШЕНИЕ

///// Create form ///// 
const element = document.body;
const formElement = document.createElement('form');
formElement.id = 'form';
element.appendChild(formElement);

////  Create Email Div  ////
const emailObj = {
  className: 'form-group',
  label: {
    for: 'email',
    text: 'Электропочта'
  },
  input: {
    className: 'form-control',
    id: 'email',
    type: 'email',
    placeholder: 'Введите свою электропочту'
  } 
}
const emailField = `
<div class="${emailObj.className}">
<label for="${emailObj.label.for}">${emailObj.label.text}</label>
<input type="${emailObj.input.type}" class="${emailObj.input.className}" id="${emailObj.input.id}" placeholder="${emailObj.input.placeholder}">
</div> `;
formElement.insertAdjacentHTML("afterbegin", emailField);

//// Create Password Div  ////
const passwordObj = {
  className: 'form-group',
  label: {
    for: 'password',
    text: 'Пароль'
  },
  input: {
    className: 'form-control',
    id: 'password',
    type: 'password',
    placeholder: 'Введите пароль'
  } 
}
const passwordField = `<div class="${passwordObj.className}">
<label for="${passwordObj.label.for}">${passwordObj.label.text}</label>
<input type="${passwordObj.input.type}" class="${passwordObj.input.className}" id="${passwordObj.input.id}" placeholder="${passwordObj.input.placeholder}">
</div>`;
formElement.insertAdjacentHTML("beforeend", passwordField);

////  Create Checkbox  ////
const checkboxObj = {
  className: ['form-group', 'form-check'],
  input: {
    id: 'exampleCheck1',
    className: 'form-check-input',
    type: 'checkbox'
  },
  label: {
    className: 'form-check-label',
    for: 'exampleCheck1',
    text: 'Запомнить меня'
  }
}
const checkboxField = `<div class="${checkboxObj.className[0]} ${checkboxObj.className[1]}">
<input type="${checkboxObj.input.type}" class="${checkboxObj.input.className}" id="${checkboxObj.input.id}">
<label class="${checkboxObj.label.className}" for="${checkboxObj.label.for}">${checkboxObj.label.text}</label>
</div>`;

formElement.insertAdjacentHTML("beforeend", checkboxField);

//// Create Button ////
const buttonObj = {
  className: ['btn', 'btn-primary'],
  type: 'submit',
  text: 'Вход'
}
const buttonElement = `<button type="${buttonObj.type}" class="${buttonObj.className[0]} ${buttonObj.className[1]}">${buttonObj.text}</button>`

formElement.insertAdjacentHTML("beforeend", buttonElement);

