///// Create form ///// 
const element = document.body;
const formElement = document.createElement('form');
formElement.id = 'form';
element.appendChild(formElement);

////  Create Email Div  ////

const formEmail = document.createElement('div');
const labelEmailForm = document.createElement('label');
const inputEmailForm = document.createElement('input');
formEmail.className = 'form-group';

labelEmailForm.innerHTML = 'Электропочта';
labelEmailForm.setAttribute('for', 'email');

inputEmailForm.className = 'form-control';
inputEmailForm.id = 'email';
inputEmailForm.setAttribute('type', 'email');
inputEmailForm.setAttribute('placeholder', 'Введите свою электропочту');

formElement.appendChild(formEmail);
formEmail.appendChild(labelEmailForm);
formEmail.appendChild(inputEmailForm);

//// Create Password Div  ////

const formPassword = document.createElement('div');
const labelPasswordForm = document.createElement('label');
const inputPasswordForm = document.createElement('input');
formPassword.className = 'form-group';

labelPasswordForm.innerHTML = 'Пароль';
labelPasswordForm.setAttribute('for', 'password');

inputPasswordForm.className = 'form-control';
inputPasswordForm.id = 'password';
inputPasswordForm.setAttribute('type', 'password');
inputPasswordForm.setAttribute('placeholder', 'Введите пароль');

formElement.appendChild(formPassword);
formPassword.appendChild(labelPasswordForm);
formPassword.appendChild(inputPasswordForm);

////  Create Checkbox  ////
const formCheckbox = document.createElement('div');
const inputCheckboxForm = document.createElement('input');
const labelCheckboxForm = document.createElement('label');
formCheckbox.className = 'form-group form-check';

labelCheckboxForm.innerHTML = 'Запомнить меня';
labelCheckboxForm.setAttribute('for', 'exampleCheck1');
labelCheckboxForm.className = 'form-check-label';

inputCheckboxForm.className = 'form-check-input';
inputCheckboxForm.id = 'exampleCheck1';
inputCheckboxForm.setAttribute('type', 'checkbox');

formElement.appendChild(formCheckbox);
formCheckbox.appendChild(inputCheckboxForm);
formCheckbox.appendChild(labelCheckboxForm);

//// Create Button ////
const formButton = document.createElement('button');
formButton.innerHTML = 'Вход';
formButton.className = 'btn btn-primary';
formButton.setAttribute('type', 'submit');
formElement.appendChild(formButton);