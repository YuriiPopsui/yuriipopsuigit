/**
 * Доработайте функцию что бы она возвращала объект из переданного вложенного массива
 * 
 * Фукнция принимает 1 аргумента
 * 1. Массив из массивов который содержит 2 элемента — [ [ element1, element2 ] ]
 * 
 * ЗАПРЕЩЕНО ИСПОЛЬЗОВАТЬ ВСТРОЕННЫЙ МЕТОД Object.fromEntries
 * 
 * Обратите внимание!
 * 1. Генерировать ошибку если второй элемент вложенного массива не число, не строка или не null
 * 2. Обязательно использовать деструктуризацию при извлечении элементов массива
 * 3. Если в качестве второго аргумента был передан массив вида [ [ element1, element2 ] ], то его так же нужно преобразовать в объект
 * 4. Для перебора массива можно воспользоваться циклом for..of.
*/

const fromEntries = (entries) => {
    const obj = {};
    // const entries = [['name', 'John'], ['age', 35], ['address', ['city', 'New York']]];
    for(let arr of entries) {
      let [arrKey, arrValue] = arr;
        if (!Array.isArray(arrValue)) {
          if (objectValidate(arrValue)) {
            obj[`${arrKey}`] = arrValue;
          }
        } 
        else {
          const obj1 = {};
          let [newArrKey, newArrValue] = arrValue;
          if (objectValidate(newArrValue)) {
            obj1[`${newArrKey}`] = newArrValue;
          }
          obj[`${arrKey}`] = obj1;
        }  
      // console.log(obj);
    }
    return obj;
};

function objectValidate(value) {
  let validate = false;
  if (typeof value === 'number' || typeof value === 'string' || value !== null) {
    validate = true;
  } 
  else {
     throw new Error('Second Element is not a String/Number/null!');
    }
 return validate;
}

console.log(fromEntries([['name', 'John'], ['age', 35]])); // { name: 'John', age: 35 }
console.log(fromEntries([['name', 'John'], ['address', ['city', 'New  York']]])); // { name: 'John', address: { city: 'New  York' } }



