/*
* Задача 2.
 *
 * Напишите скрипт, который проверяет идентичны ли массивы.
 * Если массивы полностью идентичны - в переменную flag присвоить значение true, 
 * иначе - false. 
 * 
 * Пример 1: const arr1 = [1, 2, 3];
 *           const arr2 = [1, '2', 3];
 *           flag -> false
 *
 * Пример 2: const arr1 = [1, 2, 3];
 *           const arr2 = [1, 2, 3];
 *           flag -> true
 *
 * Пример 3: const arr1 = [];
 *           const arr2 = arr1;
 *           flag -> true
 *
 * Пример 4: const arr1 = [];
 *           const arr2 = [];
 *           flag -> true
 * 
 * Условия:
 * - Обязательно проверять являются ли сравниваемые структуры массивами;
 *
*/

const arr1 = [1, 2, 3];
const arr2 = [1, 3, 3];
let flag = '';

// РЕШЕНИЕ
function isEqual(arr1, arr2) {
  if (Array.isArray(arr1) && Array.isArray(arr2)) {
    if(arr1.length === arr2.length) {
      flag = true;
    }
    else{
      return flag = false;
    }
    for(let i = 0; i < arr1.length; i++) {
      if (arr1[i] !== arr2[i]) {
        return flag = false;
      }
    }
    return flag;
  }
  else {
    throw new Error ('Is not an Array');
  }
  
}

console.log(isEqual(arr1, arr2));