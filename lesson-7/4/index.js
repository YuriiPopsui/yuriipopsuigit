/**
 * Задача 4.
 *
 * Напишите скрипт, который будет возвращать новый массив.
 * Этот новый массив должен содержать исключительно длины строк, которые были в
 * переданном массиве.
 * Если строк в переданном массиве не было — нужно вернуть пустой массив.
 *
 * Условия:
 * - Обязательно использовать встроенный метод массива filter;
 * - Обязательно использовать встроенный метод массива map.
 *
*/

const arr = [1, 
  'string', 
  false, 
  2, 
  true, 
  {
      name: 'Уолтер',
      surname: 'Уайт',
  }, 
  NaN, 
  '', 
  0, 
  undefined, 
  1, 
  'Hello', 
  true, 
  void 0, 
  () => {}, 
  ' ', 
  24, 
  null, 
  Infinity, 
  'hello']; // ИЗМЕНЯТЬ ЗАПРЕЩЕНО

// РЕШЕНИЕ

const newArr = arr.filter(item => typeof(item) === 'string').map(item => item.length);
const a = arr.filter((item) => {
  if(typeof(item) ==='object' && item !== null){
  newArr.push(Object.keys(item).filter(item => typeof(item) === 'string').map(item => item.length));
 
  newArr.push(Object.values(item).filter(item => typeof(item) === 'string').map(item => item.length));
 
  
  }
});
console.log(newArr);
