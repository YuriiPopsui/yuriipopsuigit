/**
 * Задача 2.
 *
 * Создайте функцию `f`, которая возвращает сумму всех переданных числовых аргументов.
 *
 * Условия:
 * - Функция должна принимать любое количество аргументов;
 * - Генерировать ошибку, если в качестве любого входного аргумента было предано не число.
 */

// РЕШЕНИЕ

function f() {
  let summ = 0;
  for (item of arguments) {
    if (typeof item !== "number") {
      throw new Error('Error! Item is not number!');
    }
    else {
      summ = summ + item;
    }
  }
  return summ;
}



console.log(f(1, 1, 1, 2, 1, 1, 1, 1)); // 9
console.log(f(1, 1, 1)); // 3
console.log(f(1, 1, 5, 98)); // 105
