/**
 * Примерные данные для заполнения, вы можете использовать свои данные.
 *  Имя: "Firefox",   Компания: "Mozilla",        Процент: "8.01%"        
 *  Имя: "Chrome",    Компания: "Google",         Процент: "68.26%"
 *  Имя: "Edge",      Компания: "Microsoft",      Процент: "6.67%"        
 *  Имя: "Opera",     Компания: "Opera Software", Процент: "1.31%"
 * 
 */

const dataArray = [];
const form = document.getElementById('form');
const addDataBtn = document.querySelector('input[type="submit"]');
const dataInput = document.querySelectorAll('input[type="text"]');
const nameInput = document.querySelector('input[name="browser"]');
const companyInput = document.querySelector('input[name="company"]');
const processBtn = document.querySelector('button');


dataInput.forEach((item) => {item.addEventListener('input', () => {
  if(validForm(dataInput)) {
    addDataBtn.removeAttribute('disabled');
  } else {
    addDataBtn.setAttribute('disabled','true');
  }
  });
});
const dataObject = {};
dataInput.forEach((elem) => {elem.addEventListener('input', () => {
  if(elem.name === 'browser') {
    if(validateString(elem)) {
      dataObject.name = elem.value;
    } 
  } 
  if (elem.name === 'company') {
    if(validateString(elem)) {
      dataObject.company = elem.value;
    } 
  } 
  if (elem.name === 'percent') {
    if (validateNumber(elem)) {
      dataObject.marketShare = elem.value;
    }
  }     
}
 )});

addDataBtn.addEventListener('click', (e) =>{
  e.preventDefault();
  const tempObject = Object.assign({}, dataObject);
  dataArray.push(tempObject);
  form.reset();
});


processBtn.addEventListener('click', () => {
  const finalObject = findObject(dataArray);
  document.querySelector('#result').innerHTML = `Самый востребованный браузер это ${finalObject.name} от компании ${finalObject.company} с процентом использования ${finalObject.marketShare}`
});

function findObject(array) {
  const newArray = [];
  array.forEach(item => {
    const {marketShare} = item;
    newArray.push(marketShare);
  });
  const maxIndex = newArray.indexOf(newArray.reduce((acc, curr) => acc > curr ? acc : curr));
  const finalObject = dataArray[maxIndex];
  return finalObject;
}

function validateString(data) {
  let valid = true;
  // console.log(`dataValueLength is ${data.value.length}`);
  if(typeof data.value !== 'string' || data.value.length < 3) {
    // console.log('String false');
    document.querySelector(`input[name="${data.name}"]`).classList.add('error');
    valid = false;
} else {
  document.querySelector(`input[name="${data.name}"]`).classList.remove('error');
}
  return valid;
};

function validateNumber(data) {
  let valid;
  // console.log(typeof +data.value);
  // console.log(!isNaN(data.value));
  if(!isNaN(data.value) && +data.value > 0) {
    valid = true; 
    document.querySelector(`input[name="${data.name}"]`).classList.remove('error');
    return valid;
  } else {
    document.querySelector(`input[name="${data.name}"]`).classList.add('error');
    valid = false;
    return valid;
  }
};
function validForm(arr) {
  let valid = true;
  arr.forEach(item => {
    if(item.value === '' || item.classList.value === 'error') {
      valid = false;
      return valid;
    }
  });
  return valid;
};

