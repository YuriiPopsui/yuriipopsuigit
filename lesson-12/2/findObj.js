

function findObject(data) {
  const newArray = [];
  data.forEach(item => {
    const {marketShare} = item;
    newArray.push(+marketShare);
  });
  const maxIndex = newArray.indexOf(newArray.reduce((acc, curr) => acc > curr ? acc : curr));
  const finalObject = data[maxIndex];
  return finalObject;
}
export default findObject;