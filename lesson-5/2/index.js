/*
 * Задача 2.
 *
 * Создайте объект `user`, со свойствами `name`, `surname`, `job` и `data`.
 * 
 * При чтении свойства `data` должна возвращаться строка с текстом.
 * Возвращаемая строка должна содержать текст: `Привет! Я `name` `surname` и я работаю `job` `.
 * 
 * 
 * Значения свойств `name`, `surname`, `job` в объекте `user` нужно получать из функции prompt().
 * 
 * Условия:
 * - Свойство `data` обязательно должно быть геттером. 
 * 
 * Обратите внимание!
 * - Для того что бы обратиться к свойству оъекта необходимо использовать this.name, this.surname и this.job. * 
 */

// РЕШЕНИЕ

const NAME = prompt ('Your name');
const SURNAME = prompt ('Your Surname');
const JOB = prompt ('Your Job');

const user = {
  name: NAME,
  surName: SURNAME,
  job: JOB,
  get data() {
    return `Привет! Я ${this.name} ${this.surName} и я работаю ${this.job}.`
  }
};
console.log(user.data);