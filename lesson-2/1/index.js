/**
 * Задание 1
 * 
 * Написать скрипт сложения, вычитания, умножения и деления двух чисел.
 * Результат каждой операции должен быть записан в переменную и выведен в консоль.
 * 
 * Пример: sum1 = 1
 *         sum2 = 2
 *         результуть сложения: 3
 *         результуть вычитания: -1
 *         результуть умножения: 2
 *         результуть деления: 0.5
 */
 const sum1 = prompt('Введите первое число:'); // ЗАПРЕЩЕНО МЕНЯТЬ
 const sum2 = prompt('Введите второе число:'); // ЗАПРЕЩЕНО МЕНЯТЬ
 
 // РЕШЕНИЕ


//  console.log(typeof sum1);  // string
//  console.log(typeof sum2);  // string

 let num1 = +sum1;
 let num2 = +sum2;

 let addition = num1 + num2;
 let subtraction = num1 - num2;
 let multiplicate = num1 * num2;
 let division = num1 / num2;
 console.log(`sum1 = ${sum1}`);
 console.log(`sum2 = ${sum2}`);
 console.log(`Результат сложения: ${addition}`);
 console.log(`Результат вычитания: ${subtraction}`);
 console.log(`Результат умножения: ${multiplicate}`);
 console.log(`Результат деления: ${division}`);
 